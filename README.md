This project uses a graph and the Dijkstra algorithm to solve a maze. 

First, the maze is transferred into a matrix, a 0 stands for a space and a 1 for a wall.
Then this matrix is turned into a graph by making each space a node. The connected spaces are nodes with edges, of length  1, between them.
Using Dijkstra's algorithm, the graph is turned into a final matrix where a 2 stands for a part of the shortest path.
The used algorithm could have been used more efficiently by only making nodes at intersections and giving a length to the edges between corresponding  to their distance apart. 
This can be a future adjustment.
Ultimately this final matrix is drawn onto a frame as a visual representation.

