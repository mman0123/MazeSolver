package solver;

import java.awt.Graphics;
import java.awt.Color;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * This class draws the matrix with the marked path.
 * @author Michiel
 *
 */
public class Draw extends JFrame {
	
	private final String MAZEPATH = "C:\\Users\\Michiel\\Documents\\School\\Algoritmen\\Mezelf\\MazeSolver\\maze.txt";
	private int cellSize;
	private int[][] matrix;
	private int size;
	
	private static final long serialVersionUID = 1L;

	public Draw() throws IOException {
		super();
		setTitle("MazeSolver");
		setSize(1001, 1001);
		Solve sol = new Solve(MAZEPATH);
		size = sol.getSize();
		cellSize = 1001/size;
		matrix = sol.getSolutionMatrix();
		setContentPane(new MazeDrawing());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private class MazeDrawing extends JPanel{
		
		private static final long serialVersionUID = -7552464585915373294L;

		public void paintComponent(Graphics g){
			for(int j = 0; j < size; j++) {
				for(int i = 0; i < size; i++) {
					if(matrix[i][j] == 1) {
						g.setColor(Color.blue);
					}
					else if(matrix[i][j] == 0) {
						g.setColor(Color.white);
					}
					else {
						g.setColor(Color.yellow);
					}
					g.fillRect(i*cellSize, j*cellSize, cellSize, cellSize);
				}
			}
         }
     }

     public static void main(String args[]) throws IOException{
            new Draw();
     }
}
