package solver;

import java.io.IOException;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
/**
 * Turns the matrix into a graph.
 * @author Michiel
 *
 */
public class MazeGraph extends SingleGraph {
	private MazeMatrix mazeMatrix;
	private int [][] matrix;
	private int size;
	
	/**
	 * Turns the matrix into a graph. Each space is a node. Nodes are connected by edges with length 1.
	 * @param mazePath the filepath of maze.txt
	 */
	public MazeGraph(String mazePath) throws IOException {
		super("mazeGraph");
		mazeMatrix = new MazeMatrix(mazePath);
		matrix = mazeMatrix.getMatrix();
		size = mazeMatrix.getSize();
		int nodeCode = 0;
		addNode("START", 0, findStartY());
		addNode("END", size-1, findEndY());
		Node[] leftNodes = new Node[size-2];
		leftNodes[findStartY()-1] = getNode("START");
		
		for (int i=1; i < size-1; i++) {
			for (int j=1; j < size-1; j++) {
				if (matrix[i][j] != 1) {
					nodeCode++;
					Node n = addNode(nodeCode, i, j);
					
					if(i == size-2 && j == findEndY()) {
						Edge e = addEdge(n.getId() + "END", n, getNode("END"));
						e.addAttribute("length",1);
					}
					if(matrix[i-1][j] == 0) {
						Edge e = addEdge(n.getId() + "L" + leftNodes[j-1].getId(), n, leftNodes[j-1]);
						e.addAttribute("length",1);
					}
					if(matrix[i][j-1] == 0) {
						String upperNodeId = Integer.toString(nodeCode-1);
						Edge e = addEdge(n.getId() + "U" + upperNodeId, n, getNode(upperNodeId));
						e.addAttribute("length",1);
					}
					leftNodes[j-1] = n;
				}
				else {
					leftNodes[j-1] = null;
				}
			}
		}
	matrix[0][findStartY()] = 2;
	matrix[size-1][findEndY()] = 2;
	}
	
	/**
	 * Marks a space as part of the shortest route.
	 * @param x x coordinate of the space.
	 * @param y y coordinate of the space.
	 */
	public void setRoute(int x,int y) {
		matrix[x][y] = 2;
	}
	
	/**
	 * 
	 * @return the matrix
	 */
	public int[][] getMatrix() {
		return matrix;
	}
	
	/**
	 * 
	 * @return the size of the matrix
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * 
	 * @return y value of the startnode
	 */
	private int findStartY() {
		for(int j=1; j < size-1; j++) {
			if(matrix[0][j] == 0) return j;
		}
		return 0;
	}
	
	/**
	 * 
	 * @returny value of the endnode
	 */
	private int findEndY() {
		for(int j=1; j < size-1; j++) {
			if(matrix[size-1][j] == 0) return j;
		}
		return 0;
	}
	
	/**
	 *  Adds a node to the graph.
	 * @param nodeCode node id
	 * @param x coordinate of the space
	 * @param y coordinate of the space
	 */
	private Node addNode(int nodeCode, int x, int y) {
		return addNode(Integer.toString(nodeCode),x,y);
	}
	
	private Node addNode(String nodeId, int x, int y) {
		Node n = addNode(nodeId);
		n.addAttribute("xPos",x);
		n.addAttribute("yPos",y);
		return n;
	}
	
}
