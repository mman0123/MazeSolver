package solver;

import java.io.IOException;
import org.graphstream.graph.Node;
import org.graphstream.algorithm.Dijkstra;
/**
 * Searches the shortest path in the graph and returns a matrix in which this path is marked.
 * @author Michiel
 *
 */
public class Solve extends Dijkstra {
	private MazeGraph graph;
	
	/**
	 * Searches the shortest path.
	 * @param mazePath path of the maze.txt file
	 */
	public Solve(String mazePath) throws IOException {
		super(Dijkstra.Element.EDGE,null,"length");
		graph = new MazeGraph(mazePath);
		init(graph);
		setSource(graph.getNode("START"));
		compute();
		
		//graph.display(); //Only works for relatively small mazes.
		
		for(Node n : getPathNodes(graph.getNode("END"))) {
			int x = n.getAttribute("xPos");
			int y = n.getAttribute("yPos");
			graph.setRoute(x,y);
		}
	}
	
	/**
	 * 
	 * @return the size of the matrix
	 */
	public int getSize() {
		return graph.getSize();
	}
	
	/**
	 * 
	 * @return the matrix with the shortest path marked
	 */
	public int[][] getSolutionMatrix() {
		return graph.getMatrix();
	}
}
