package solver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
/**
 * Class that turns the text file into a corresponding matrix
 * @author Michiel
 *
 */
public class MazeMatrix {
	private int size = 0;
	private int[][] matrix;
	
	/**
	 * Constructs a matrix corresponding to a maze.txt file
	 * @param mazePath the path where the maze.txt file is stored
	 */
	public MazeMatrix(String mazePath) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(mazePath));
		br.mark(1);
		size = br.readLine().length();
		br.reset();
	
		matrix = new int[size][size];
		for(int j = 0; j < size; j++) {
			for(int i = 0; i < size; i++) {
				if(br.read() == 88) {
					matrix[i][j] = 1;
				}
				else {
					matrix[i][j] = 0;
				}
			}
			br.skip(2);
		}
		br.close();
	}
	/**
	 * 
	 * @return the matrix
	 */
	public int[][] getMatrix() {
		return this.matrix;
	}
	/**
	 * 
	 * @return the size of the matrix
	 */
	public int getSize() {
		return size;
	}
	
}
